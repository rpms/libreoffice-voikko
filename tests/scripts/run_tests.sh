#!/bin/bash

# Check whether the libreoffice-voikko extension is installed.
# exit with 0 if yes, exit with 1 if no:

unopkg list --bundled | grep 'Identifier.*voikko'
exit $?
